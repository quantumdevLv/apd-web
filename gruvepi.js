
var GrovePi = require('node-grovepi').GrovePi

module.exports = {
    init: function(io) {

        var Commands = GrovePi.commands
        var Board = GrovePi.board

        var DHTDigitalSensor = GrovePi.sensors.DHTDigital

        var board = new Board({
            debug: true,
            onError: function(err) {
                console.log('Something wrong just happened')
                console.log(err)
            },

            onInit: function (res) {
                if (res) {
                    console.log('GrovePi Version :: ' + board.version())

                    var dhtSensor = new DHTDigitalSensor(7, DHTDigitalSensor.VERSION.DHT11, DHTDigitalSensor.CELSIUS)
                    console.log('DHT Digital Sensor (start watch)')
                    dhtSensor.on('change', function (res) {
                        console.log('DHT onChange value=' + res[1])
                        console.log('Temperature=' + res[0] + ' Humidity=' + res[1] + '%')
                        io.emit('newData', {
                            temperature: res[0],
                            humiditi: res[1]
                        })
                        const MongoClient = require('mongodb').MongoClient;
  const assert = require('assert');

  var url = "mongodb://breather:kRv56F3YeskchtA@46.101.134.213/breather";
  MongoClient.connect(url, function(err, db) {   //here db is the client obj
      if (err) throw err;
      var dbase = db.db("breather"); //here
      

  dbase.collection("soket").insertMany([{Temperature:res[0]}, { Humiditi:res[1]}], function(err, r) {
      assert.equal(null, err);
      assert.equal(2, r.insertedCount);
        console.log("success")
      db.close();
    });
  });
                    })

                    dhtSensor.watch(500) // milliseconds
                }
            }
        })
        board.init()

    }
}
 