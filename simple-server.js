//import express package
var express = require("express");

//import mongodb package
var mongodb = require("mongodb");

//MongoDB connection URL - mongodb://host:port/dbName
var dbHost = "mongodb://breather:kRv56F3YeskchtA@46.101.134.213/breather";

//DB Object
var dbObject;

//get instance of MongoClient to establish connection
var MongoClient = mongodb.MongoClient;

//Connecting to the Mongodb instance.
//Make sure your mongodb daemon mongod is running on port 27017 on localhost
MongoClient.connect(dbHost, function(err, db){
  if ( err ) throw err;
  dbObject = db;
});

function getData(responseObj){
  //use the find() API and pass an empty query object to retrieve all records
  dbObject.collection("soket").find({}).toArray(function(err, docs){
    if ( err ) throw err;
    var temperatureArray = [];
    var humiditiArray = [];
    

    for ( index in docs){
      var doc = docs[index];
      //category array
      var temperature = doc['Temperature'];
      //series 1 values array
      var humiditi = doc['Humiditi'];
      //series 2 values array
      
      
      temperatureArray.push({"value" : temperature});
      humiditiArray.push({"value" : humiditi});
    }

    var dataset = [
      {
        "seriesname" : "Temperature",
        "data" : temperatureArray
      },
      {
        "seriesname" : "Humiditi",
        "data": humiditiArray
      }
    ];

    var response = {
      "dataset" : dataset,
      "categories" : temperatureArray
    };
    responseObj.json(response);
  });
}

//create express app
var app = express();

//NPM Module to integrate Handlerbars UI template engine with Express
var exphbs  = require('express-handlebars');

//Declaring Express to use Handlerbars template engine with main.handlebars as
//the default layout
app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

//Defining middleware to serve static files
app.use('/public', express.static('public'));
app.get("/sensor", function(req, res){
  getData(res);
});
app.get("/", function(req, res){
  res.render("chart");
});

app.listen("3000", function(){
  console.log('Server up: http://localhost:3300');
});

